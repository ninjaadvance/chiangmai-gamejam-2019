﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HordeSpawner : MonoBehaviour
{
    [SerializeField]
    private HordeSlot[] hordes;
    [SerializeField]
    private float initialDelay;
    [SerializeField]
    private float multiplier;
    [SerializeField]
    private float multiplyEvery;
    [SerializeField]
    private float minCooldown;
    [SerializeField]
    private float maxCooldown;
    [SerializeField]
    private Stage stage;
    private float elapsed;
    private int multipliedCount;
    private float cooldown;
    private void Awake()
    {
        elapsed = 0;
        multipliedCount = 0;
        cooldown = initialDelay;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void Spawn()
    {
        List<Enemy> outgoing = new List<Enemy>();
        for (int i = 0; i < multipliedCount + 1; i++)
        {
            foreach (HordeSlot h in hordes)
            {
                outgoing.Add(h.SelectEnemy());
            }
        }
        int row = 0;
        int countInRow = 0;
        for (int i = 0; i < outgoing.Count; i++)
        {
            if (countInRow > row)
            {
                countInRow = 0;
                row++;
            }
            Enemy e = Instantiate(outgoing[i], transform);
            e.transform.localPosition = new Vector3(countInRow * 0.5f - (row * 0.5f / 2), 0, row);
            e.transform.SetParent(null, true);
            countInRow++;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (stage.IsGameOver)
            return;

        elapsed += Time.deltaTime;
        if (Mathf.FloorToInt((Time.timeSinceLevelLoad - initialDelay) / multiplyEvery) > multipliedCount)
        {
            multipliedCount++;
        }
        if (elapsed > cooldown)
        {
            Spawn();
            elapsed = 0;
            cooldown = UnityEngine.Random.Range(minCooldown, maxCooldown);
        }
    }
}
[Serializable]
public class HordeSlot
{
    public Enemy enemy1;
    [Range(0, 100)]
    public int enemy1Chance = 100;
    public Enemy enemy2;
    public Enemy SelectEnemy()
    {
        if (UnityEngine.Random.Range(0, 100) < enemy1Chance) return enemy1;
        else return enemy2;
    }
}