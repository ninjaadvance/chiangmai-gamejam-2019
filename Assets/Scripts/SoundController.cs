﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController:MonoBehaviour
{
    [SerializeField]
    AudioSource[] audioSource;


    public void Play(AudioClip clip)
    {
        this.audioSource[0].clip = clip;
        this.audioSource[0].Play();

    }
}
