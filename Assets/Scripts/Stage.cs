﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Stage : MonoBehaviour
{
    [SerializeField]
    private GameObject gameOverUi;
    [SerializeField]
    private TextMeshProUGUI scoreText;
    [SerializeField]
    private TextMeshProUGUI uiScoreText;
    public bool IsGameOver => gameOverUi.activeSelf;
    private int score;

    private void Awake()
    {
        score = 0;
        uiScoreText.text = "Score: 0";
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void AddScore(int score)
    {
        this.score += score;
        uiScoreText.text = "Score: " + this.score;
    }

    public void ShowGameOver()
    {
        scoreText.text = "Score: " + score;
        gameOverUi.SetActive(true);
        StartCoroutine(CountDownAndReset());
    }
    private IEnumerator CountDownAndReset()
    {
        yield return new WaitForSeconds(3);
        yield return SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
