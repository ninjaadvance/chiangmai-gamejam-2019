﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Manoeuvre;
using TMPro;

public class Player : MonoBehaviour
{
    [SerializeField]
    private new Camera camera;
    [SerializeField]
    private float maxFov;
    [SerializeField]
    private float minFov;
    [SerializeField]
    private ThrowableObject defaultObjectPrototype;
    [SerializeField]
    private float launchForce;
    [SerializeField]
    private float throwCooldown;
    [SerializeField]
    private Transform throwPositionRight;
    [SerializeField]
    private Transform throwPositionLeft;
    [SerializeField]
    private Transform rightHand;
    [SerializeField]
    private Transform leftHand;
    [SerializeField]
    private ManoeuvreFPSController controller;
    [SerializeField]
    private Stage stage;
    [SerializeField]
    private TextMeshProUGUI healthText;
    [SerializeField]
    private SpriteRenderer rightHandSprite;
    [SerializeField]
    private SpriteRenderer leftHandSprite;
    [SerializeField]
    private ParticleSystem healParticle;
    private float lastThrowLeft;
    private float lastThrowRight;
    private Vector3 rightHandStartPosition;
    private Vector3 leftHandStartPosition;
    private Coroutine moveHandCoroutine;
    private ThrowableObject currentThrowablePrototype;
    private float fovChanging;
    private void Awake()
    {
        lastThrowRight = 0;
        lastThrowLeft = 0;
        rightHandStartPosition = rightHand.localPosition;
        leftHandStartPosition = leftHand.localPosition;
        rightHandSprite.sprite = defaultObjectPrototype.Sprite;
        leftHandSprite.sprite = defaultObjectPrototype.Sprite;
        currentThrowablePrototype = defaultObjectPrototype;
    }
    public void Heal(int amount)
    {
        controller.Health.currentHealth = Mathf.Min(controller.Health.currentHealth + amount, controller.Health.Health);
        healthText.text = "Health: " + controller.Health.currentHealth;
        healParticle.Play();
    }
    public void SetThrowableObject(ThrowableObject o)
    {
        Debug.Assert(o != null);
        currentThrowablePrototype = o;
        rightHandSprite.sprite = o.Sprite;
        leftHandSprite.sprite = o.Sprite;
        MoveHand(leftHand);
        MoveHand(rightHand);
    }
    // Start is called before the first frame update
    void Start()
    {
        healthText.text = "Health: " + controller.Health.currentHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.Health.currentHealth <= 0)
            return;

        if (Input.GetMouseButtonDown(1) && Time.timeSinceLevelLoad >= lastThrowRight + throwCooldown)
        {
            lastThrowRight = Time.timeSinceLevelLoad;
            ThrowableObject o = Instantiate(currentThrowablePrototype, throwPositionRight.position, throwPositionRight.rotation);
            Vector3 launchVector = camera.transform.TransformDirection(Vector3.forward);
            launchVector *= launchForce;
            launchVector.y += 2;
            o.Launch(launchVector);
            MoveHand(rightHand);
            fovChanging = -10;
        }
        if (Input.GetMouseButtonDown(0) && Time.timeSinceLevelLoad >= lastThrowLeft + throwCooldown)
        {
            lastThrowLeft = Time.timeSinceLevelLoad;
            ThrowableObject o = Instantiate(currentThrowablePrototype, throwPositionLeft.position, throwPositionLeft.rotation);
            Vector3 launchVector = camera.transform.TransformDirection(Vector3.forward);
            launchVector *= launchForce;
            launchVector.y += 2;
            o.Launch(launchVector);
            MoveHand(leftHand);
            fovChanging = -10;
        }

        //camera.fieldOfView = Mathf.Clamp(camera.fieldOfView + fovChanging * Time.deltaTime, minFov, maxFov);
        fovChanging += 15 * Time.deltaTime;
        fovChanging = Mathf.Min(fovChanging, 15);
    }
    private void MoveHand(Transform hand)
    {
        StartCoroutine(MoveHandCoroutine(hand));
    }
    public void TakeDamage(int damage)
    {
        controller.Health.currentHealth = Mathf.Max(controller.Health.currentHealth - damage, 0);
        healthText.text = "Health: " + controller.Health.currentHealth;

        if (controller.Health.currentHealth <= 0)
        {
            stage.ShowGameOver();
        }
    }
    private IEnumerator MoveHandCoroutine(Transform hand)
    {
        Vector3 start;
        Vector3 end;
        if (hand == rightHand)
        {
            start = new Vector3(rightHandStartPosition.x, rightHandStartPosition.y - 2f, rightHandStartPosition.z - 0.8f);
            end = rightHandStartPosition;
        }
        else
        {
            start = new Vector3(leftHandStartPosition.x, leftHandStartPosition.y - 2f, leftHandStartPosition.z - 0.8f);
            end = leftHandStartPosition;
        }
        hand.localPosition = start;
        float duration = throwCooldown;
        float elapsed = 0;
        while (elapsed <= duration)
        {
            elapsed += Time.deltaTime;
            hand.localPosition = Vector3.Lerp(start, end, elapsed / duration);
            yield return null;
        }
        hand.localPosition = end;
        moveHandCoroutine = null;
    }
}
