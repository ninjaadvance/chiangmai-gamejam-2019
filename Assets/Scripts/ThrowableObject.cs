﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableObject : MonoBehaviour
{
    [SerializeField]
    private new Rigidbody rigidbody;
    [SerializeField]
    private int damage;
    [SerializeField]
    private SpriteRenderer sprite;
    [SerializeField]
    private float explosionRadius;
    [SerializeField]
    private Effect touchEffect;  
    private Vector3 startForce;
    public Sprite Sprite => sprite.sprite;
    public void Launch(Vector3 force)
    {
        startForce = force;
        rigidbody.AddForce(force, ForceMode.VelocityChange);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (explosionRadius > 0)
        {
            Collider[] area = Physics.OverlapSphere(transform.position, explosionRadius);
            foreach (Collider c in area)
            {
                if (c.tag != "Enemy")
                    continue;

                Enemy e = c.GetComponent<Enemy>();
                e.TakeDamage(damage, startForce.normalized, Vector3.zero);
            }
            if (touchEffect != null)
            {
                Effect t = Instantiate(touchEffect, transform.position, Quaternion.identity);
            }
            Destroy(gameObject);
        }
        else
        {
            if (collision.transform.tag == "Floor")
            {
                Destroy(gameObject, 3.0f);
            }
            else if (collision.transform.tag == "Enemy")
            {
                Enemy e = collision.transform.GetComponent<Enemy>();
                e.TakeDamage(damage, startForce.normalized, collision.GetContact(0).point);
                Destroy(gameObject);
                if (touchEffect != null)
                {
                    Effect t = Instantiate(touchEffect, transform.position, Quaternion.identity);
                }
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
