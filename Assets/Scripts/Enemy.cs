﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private int maxHp;
    [SerializeField]
    private new Rigidbody rigidbody;
    [SerializeField]
    private new Collider collider;
    [SerializeField]
    private int score;
    
    [SerializeField]
    private float speed;
    [SerializeField]
    private int attackPower;
	
	[SerializeField]
	private AudioClip hitSound;
	[SerializeField]
	private AudioClip deadSound;

    private GameObject player;
    private Stage stage;
    private int hp;
    private SoundController soundController;

    private NavMeshAgent navMeshAgent;

    private Animator anim;

    public int Score => score;

    public bool disableNavMesh = false;

    private void Awake()
    {
        hp = maxHp;
		player = GameObject.FindGameObjectWithTag("Player");
        var s = GameObject.FindGameObjectWithTag("Sound");
        soundController = s.transform.GetComponent<SoundController>();
        anim = GetComponent<Animator>();
        stage = FindObjectOfType<Stage>();
        if (disableNavMesh) {
            return;
        }
        navMeshAgent = this.gameObject.AddComponent<NavMeshAgent>();
    }
    // Start is called before the first frame update

    private void Start()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
	{
		if (collision.transform.tag == "Player")
		{
			Player p = collision.transform.GetComponent<Player>();
			p.TakeDamage(attackPower);

		}
	}


    // Update is called once per frame
    void Update()
    {

        if (hp <= 0)
			return;


		Vector3 planarTarget = new Vector3(player.transform.position.x, 0, player.transform.position.z);
		Vector3 planarPosition = new Vector3(transform.position.x, 0, transform.position.z);
		transform.LookAt(planarTarget, Vector3.up);
        
        //navMeshAgent.destination = player.transform.position;
        if (disableNavMesh)
        {
            transform.Translate((planarTarget - planarPosition).normalized * speed * Time.deltaTime, Space.World);
            return;
        }
        navMeshAgent.speed = speed;
        navMeshAgent.SetDestination(player.transform.position);
        //transform.Translate((planarTarget - planarPosition).normalized * speed * Time.deltaTime, Space.World);

    }

	public void TakeDamage(int damage, Vector3 direction, Vector3 pointOfContact)
	{
        if (hp <= 0)
            return;

		hp -= damage;
		if (hp <= 0)
		{
			this.soundController.Play(deadSound);

			rigidbody.isKinematic = false;
			rigidbody.constraints = RigidbodyConstraints.None;
			collider.enabled = false;
			direction *= 20;
			direction.y = 10;
			rigidbody.AddForceAtPosition(direction, pointOfContact, ForceMode.VelocityChange);
            if (anim != null)
            {
                anim.SetBool("isDead", true);
            }

            Destroy(gameObject, 1.0f);
		}
		else
		{
			this.soundController.Play(hitSound);
		}
	}
}