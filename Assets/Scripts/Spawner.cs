﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private ThrowableObject[] items;
    [SerializeField]
    private float minCooldown;
    [SerializeField]
    private float maxCooldown;
    [SerializeField]
    private Player player;
    [SerializeField]
    private new SpriteRenderer renderer;
    [SerializeField]
    private ParticleSystem particle;
    private float collectedAt;
    private float cooldown;
    private ThrowableObject item;
    private void Awake()
    {
        cooldown = 0;
        if (items.Length != 0)
        {
            item = items[Random.Range(0, items.Length)];
            renderer.sprite = item.Sprite;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        particle.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player" || !renderer.enabled)
            return;

        renderer.enabled = false;
        particle.Stop();
        collectedAt = Time.timeSinceLevelLoad;
        cooldown = Random.Range(minCooldown, maxCooldown);
        if (item != null)
        {
            player.SetThrowableObject(item);
        }
        else
        {
            player.Heal(10);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!renderer.enabled && Time.timeSinceLevelLoad >= collectedAt + cooldown)
        {
            renderer.enabled = true;
            particle.Play();
            if (items.Length != 0)
            {
                item = items[Random.Range(0, items.Length)];
                renderer.sprite = item.Sprite;
            }
        }
        else if (renderer.enabled)
        {
            renderer.transform.rotation = Quaternion.LookRotation(renderer.transform.position - player.transform.position);
        }
    }
}
